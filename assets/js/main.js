$(document).ready(function(){
	$('.owl-carousel').owlCarousel({
		loop: true,
		nav: true,
		items: 1,
		//autoplay: true,
		autoplayTimeout: 5000,
		navText: ['<img src="assets/images/slider-arrow-left.png">','<img src="assets/images/slider-arrow-right.png">']
	});

	$('.menu-toggle').on('click', function(){
		$('.header-links').toggleClass('dropdown');
	});
});