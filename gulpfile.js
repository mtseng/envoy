//npm install gulp gulp-sass gulp-sourcemaps gulp-minify-css gulp-rename browser-sync gulp-uglify --save-dev
var gulp = require('gulp'),
	sass = require('gulp-sass'),
	sourcemaps = require('gulp-sourcemaps'),
	minifycss = require('gulp-minify-css'),
	rename = require('gulp-rename'),
	uglify = require('gulp-uglify'),
	browserSync = require('browser-sync');

gulp.task('browser-sync', function(){
	browserSync.init({
		server: {
			baseDir: './'
		}
	});
});


gulp.task('sass', function(){
	gulp.src('./assets/scss/main.scss')
		.pipe(sourcemaps.init())
		.pipe(sass().on('error',sass.logError))
		.pipe(rename({suffix: '.min'}))
		.pipe(minifycss())
		.pipe(sourcemaps.write())
		.pipe(gulp.dest('dist/css'))
		.pipe(browserSync.stream());
});

gulp.task('js', function(){
	gulp.src('./assets/js/main.js')
		.pipe(uglify())
		.pipe(rename({suffix: '.min'}))
		.pipe(gulp.dest('dist/js'))
		.pipe(browserSync.stream());
});

gulp.task('build', ['sass','js'])

gulp.task('default', function(){

	//use this line for just local browser sync
	browserSync.init({
		server: './'
	});

	//use this line for mamp/local php
	// browserSync.init(null,{
	// 	proxy: 'http://localhost:8888'
	// })

	gulp.watch('**/*.{html,php}').on('change', browserSync.reload);
	gulp.watch('./assets/scss/**/*.{scss,sass}', ['sass']);
	gulp.watch('./assets/js/**/*.js', ['js']);
});